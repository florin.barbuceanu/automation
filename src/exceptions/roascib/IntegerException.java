package exceptions.roascib;

public class IntegerException {
    public static double circleArea(int raza) throws MyCustomException {
        if (raza<=0){
            throw new MyCustomException("Raza need to be positive");
        } else {
            return Math.pow(raza,2)* Math.PI;
        }
    }
    public static void main(String[] args) {
        try {
            System.out.println(5/0);
            Integer a = Integer.parseInt(args[0]);
            System.out.println("Aria cercului este: "+circleArea(a));
        } catch (NumberFormatException|ArrayIndexOutOfBoundsException ex) {
            TestDebug.printMessage("We need to enter a number");
        } catch (MyCustomException e) {
            System.out.println(e.getMessage());
            e.printCustomMesasge();
        } finally {
            TestDebug.printMessage("This is the area that will me run all time/For cleanup");
        }

    }
}
