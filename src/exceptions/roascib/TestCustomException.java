package exceptions.roascib;

public class TestCustomException {
    public static void main(String[] args) throws MyCustomException {
        int amount;
        try {
            amount = Integer.parseInt(args[0]);
            if (amount < 10)
                throw new MyCustomException("No enough amount to make transaction");
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            System.out.println("Number should be int");
        } catch (IllegalArgumentException ex) {
            System.out.println("Invalid argument");
        } finally {
            System.out.println("Finally block - I'm always executed");
        }
    }
}
