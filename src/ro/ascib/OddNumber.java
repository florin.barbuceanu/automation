package ro.ascib;

public class OddNumber {

    public static boolean isOddNumber(int nr) {
        if (nr % 2 == 0)
            return false;
        else
            return true;
    }

    public static void main(String[] args) {
        int nr;
        nr = Integer.parseInt(args[0]);
        System.out.println("Number "+nr+" is odd?:"+isOddNumber(nr));
    }
}
