package ro.ascib;

public class Square {

    private double latura;

    public Square() {
    }

    public Square(double latura) {
        this.latura = latura;
    }

    public void setLatura(double latura) throws Exception {
        if (latura <= 0)
            throw new Exception("Latura need to be positive");
        else
            this.latura = latura;
    }

    public double getLatura() {

        return latura;
    }

    public double getArea() {
        return Math.pow(getLatura(), 2);
    }
}
