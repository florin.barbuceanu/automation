package collection.ascib;

public class Student extends Person {
    public String profession;

    public Student(String firstName, String lastName) {
        super(firstName, lastName);
        this.profession = "Student";
    }

    @Override
    public String toString() {
        return "Student{" +
                "profession='" + profession + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
