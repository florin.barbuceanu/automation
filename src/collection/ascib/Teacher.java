package collection.ascib;

public class Teacher extends Person {
    public String profession, curriculumType;

    public Teacher(String firstName, String lastName, String curriculumType) {
        super(firstName, lastName);
        this.profession = "Teacher";
        this.curriculumType = curriculumType;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "profession='" + profession + '\'' +
                ", curriculumType='" + curriculumType + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
