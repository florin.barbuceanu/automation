package collection.ascib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercises {
    public static void listContent(List<Person> listPerson) {
        for (Person p : listPerson) {
            System.out.println(p.firstName + " " + p.lastName);
        }
    }
    public static void listStudents(List<Student> listPerson) {
        for (Student p : listPerson) {
            System.out.println(p.toString());
        }
    }

    public static void listTeachers(List<Teacher> listPerson) {
        for (Teacher p : listPerson) {
            System.out.println(p.toString());
        }
    }

    public static void main(String[] args) {
        List<Person> listPerson = new ArrayList<Person>();
        listPerson.add(new Person("Vasile", "Popescu"));
        listPerson.add(new Person("Marian", "Ionescu"));
        listPerson.add(new Person("Maria", "Bitanescu"));
        listContent(listPerson);
        listPerson.remove(1);
        System.out.println("*After Remove:");
        listContent(listPerson);

//       define map
        Map<PersonType,Object> listOfPersonsType = new HashMap<>();
//        add student and teacher
        listOfPersonsType.put(PersonType.STUDENT, new Student("Ion","Vasile"));
        listOfPersonsType.put(PersonType.TEACHER, new Teacher("Ioana","Popescu","Math"));
        listOfPersonsType.put(PersonType.STUDENT, new Student("Alex","Gatu"));

//      print content of map
        System.out.println("\nMap Content:");

        for(PersonType p: listOfPersonsType.keySet()) {
            System.out.println(listOfPersonsType.get(p).toString());
        }

        System.out.println("\nMap Content with Entry:");
//       list map content using HashMap
        for(Map.Entry entry : listOfPersonsType.entrySet() )
            System.out.println(entry.getKey()+" "+entry.getValue());

//        define hashmap with list
        List<Student> listOfStudents = new ArrayList<Student>();
        List<Teacher> listOfTheachers = new ArrayList<Teacher>();

        Map <PersonType,Object> listWithPersonsType = new HashMap<>();

        listOfStudents.add(new Student("Adina","Elena"));
//       add students on map
        listWithPersonsType.put(PersonType.STUDENT,listOfStudents);
        listOfStudents.add(new Student("Stancu","Marian"));
//        add teachers on map
        listOfTheachers.add(new Teacher("Lucian","Stancu", "Sport"));
        listOfTheachers.add(new Teacher("Andrei", "Ionescu", "Info"));
        listWithPersonsType.put(PersonType.TEACHER, listOfTheachers);

//        list the content of the hashmap
        System.out.println("\nMap with list content:");
        System.out.println(listWithPersonsType.toString());
        for(PersonType entry : listWithPersonsType.keySet()){
            if (entry == PersonType.STUDENT) {
                System.out.println("Students:");
                ArrayList<Student> st = (ArrayList<Student>) listWithPersonsType.get(entry);
                listStudents(st);
            } else if (entry == PersonType.TEACHER){
                System.out.println("Teachers:");
                ArrayList<Teacher> st1 = (ArrayList<Teacher>) listWithPersonsType.get(PersonType.TEACHER);
                listTeachers(st1);
            }
        }
    }
}
