package ro.ascib;

import org.junit.Test;
import ro.ascib.OddNumber;

import static junit.framework.TestCase.*;

public class OddNumberTest {

    @Test
    public void isOddNumberTest() {
        OddNumber nr = new OddNumber();
        assertFalse(nr.isOddNumber(4));
    }
    @Test
    public void oddNumberTestInvalid() {
        OddNumber nr = new OddNumber();
        assertTrue(nr.isOddNumber(5));
    }
}