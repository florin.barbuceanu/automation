package ro.ascib;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SquareTest {
    Square sq1;
    @Before
    public void setSquare(){
        sq1 = new Square();
    }
    @Test
    public void setLaturaPositive() {
        try {
            sq1.setLatura(10);
            assertEquals(10,sq1.getLatura(),0);
        } catch (Exception e) {
            fail( "My method should not throw an exception");
        }
    }

    @Test(expected = Exception.class)
    public void setLaturaNegative() throws Exception {
        sq1.setLatura(-2);
    }

    @Test(expected = Exception.class)
    public void setLaturaValidateExceptionMessage() throws Exception{
        try {
            sq1.setLatura(0);
        } catch (Exception re) {
            assertEquals("Latura need to be positive", re.getMessage());
            throw re;
        }
    }

    @Test
    public void getArea() {
    }
//    to do - add @After exemple
}